# Kasa

## Template

- Replace words that are "Kasa" with whatever the project name is.
- Always make sure the primary branch name is "main".
- Branch protections should be on for the main branch.
- Remove the "--dry-run" from .gitlab-ci.yml (Enables npm publishing)
- Don't forget to remove this part too!

**This template was inspired from the [sapphire-template!](https://github.com/sapphiredev/sapphire-template)**

## Features

## Docs

Auto-generated from TypeDoc [documentation]

## Licensed Under the MIT License
